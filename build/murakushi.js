(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
    typeof define === 'function' && define.amd ? define(['exports'], factory) :
    (factory((global.murakushi = {})));
}(this, (function (exports) { 'use strict';

    class vec2 extends Float32Array {

        constructor(x, y) { super(2);
            this[0] = x;
            this[1] = y;
        }

        setCoords(x, y) { this[0] = x; this[1] = y; return this; }
        resetCoords() { this[0] = 0; this[1] = 0; return this; }

        toString() {
            return `vec2(${
            this[0].toFixed(2) }, ${
            this[1].toFixed(2)
        })`;
        }

        static get ZERO() {
            return new vec2(0, 0);
        }

        static get I() {
            return new vec2(1, 0);
        }

        static get J() {
            return new vec2(0, 1);
        }

        static get UNIT() {
            return new vec2(1, 1);
        }

        assign(other) {
            this[0] = other[0];
            this[1] = other[1];
            return this;
        }

        clone() {
            return new vec2(this[0], this[1]);
        }

        static lerp(o, u, v, t) {
            const s = 1 - t;
            o[0] = u[0] * s + v[0] * t;
            o[1] = u[1] * s + v[1] * t;
            return o;
        }

        static addScaled(o, a, b, q) {
            o[0] = a[0] + b[0] * q;
            o[1] = a[1] + b[1] * q;
            return o;
        }

        addScaled(u, q) {
            this[0] += u[0] * q;
            this[1] += u[1] * q;
            return this;
        }

        //#region Dot, and distance
        static dot(u, v) { return u[0] * v[0] + u[1] * v[1]; }
        static dist(u, v) { return Math.hypot(u[0] - v[0], u[1] - v[1]); }
        static dist2(u, v) { return (u[0] - v[0]) **2 + (u[1] - v[1]) ** 2; }
        //#endregion

        //#region Magnitude, and normalization.
        get magnitude() { return Math.hypot(this[0], this[1]); }
        get magnitude2() { return this[0] ** 2 + this[1] ** 2; }
        get normalized() {
            const q = this.magnitude2;
            if (q === 0) return new vec2(0, 0);
            const r = 1 / Math.sqrt(q);
            return new vec2(u[0] * r, u[1] * r);
        }
        static normalize(o, u) {
            const q = u.magnitude2;
            if (q === 0) return o.resetCoords();
            const r = 1 / Math.sqrt(q);
            return this.setCoords(u[0] * r, u[1] * r);
        }
        normalize() {
            const q = this.magnitude2;
            if (q === 0) return this.resetCoords();
            return this.scale(1 / Math.sqrt(q));
        }
        //#endregion

        static cross(o, u, v) {
            o[0] = o[1] = 0;
            o[2] = u[0] * v[1] - u[1] * v[0];
            return o;
        }

        static perpendicular(o, u) {
            o[0] = u[0];
            o[1] = -u[1];
            return o;
        }

        //#region Static arithmetic operations
        static add(o, u, v) { o[0] = u[0] + v[0]; o[1] = u[1] + v[1]; return o; }
        static sub(o, u, v) { o[0] = u[0] - v[0]; o[1] = u[1] - v[1]; return o; }
        static mul(o, u, v) { o[0] = u[0] * v[0]; o[1] = u[1] * v[1]; return o; }
        static div(o, u, v) { o[0] = u[0] / v[0]; o[1] = u[1] / v[1]; return o; }
        static scale(o, u, q) { o[0] = u[0] * q; o[1] = u[1] * q; return o; }
        static negate(o, u) { o[0] = -u[0]; o[1] = -u[1]; return o; }
        //#endregion

        //#region Instance arithmetic operations
        add(u) { this[0] += u[0]; this[1] += u[1]; return this; }
        sub(u) { this[0] -= u[0]; this[1] -= u[1]; return this; }
        mul(u) { this[0] *= u[0]; this[1] *= u[1]; return this; }
        div(u) { this[0] /= u[0]; this[1] /= u[1]; return this; }
        scale(q) { this[0] *= q; this[1] *= q; return this; }
        negate() { this[0] = -this[0]; this[1] = -this[1]; return this; }
        //#endregion

        //#region Coords getters & setters
        get x() { return this[0]; }
        get y() { return this[1]; }
        set x(value) { this[0] = value; }
        set y(value) { this[1] = value; }
        //#endregion

    }

    class vec3 extends Float32Array {

        constructor(x, y, z) { super(3);
            this[0] = x;
            this[1] = y;
            this[2] = z;
        }

        setCoords(x, y, z) { this[0] = x; this[1] = y; this[2] = z; return this; }
        resetCoords() { this[0] = 0; this[1] = 0; this[2] = 0; return this; }

        toString() {
            return `vec3${
            this[0].toFixed(2) }, ${
            this[1].toFixed(2) }, ${
            this[2].toFixed(2)
        })`;
        }

        static get ZERO() {
            return new vec3(0, 0, 0);
        }

        static get I() {
            return new vec3(1, 0, 0);
        }

        static get J() {
            return new vec3(0, 1, 0);
        }

        static get K() {
            return new vec3(0, 0, 1);
        }

        static get UNIT() {
            return new vec3(1, 1, 1);
        }

        assign(other) {
            this[0] = other[0];
            this[1] = other[1];
            this[2] = other[2];
            return this;
        }

        clone() {
            return new vec3(this[0], this[1], this[2]);
        }

        static lerp(o, u, v, t) {
            const s = 1 - t;
            o[0] = u[0] * s + v[0] * t;
            o[1] = u[1] * s + v[1] * t;
            o[2] = u[2] * s + v[2] * t;
            return o;
        }

        static addScaled(o, a, b, q) {
            o[0] = a[0] + b[0] * q;
            o[1] = a[1] + b[1] * q;
            o[2] = a[2] + b[2] * q;
            return o;
        }

        addScaled(u, q) {
            this[0] += u[0] * q;
            this[1] += u[1] * q;
            this[2] += u[2] * q;
            return this;
        }

        static cross(o, u, v) {
            o[0] = u[1] * v[2] - u[2] * v[1];
            o[1] = u[2] * v[0] - u[0] * v[2];
            o[2] = u[0] * v[1] - u[1] * v[0];
            return o;
        }

        //#region Dot, and distance
        static dot(u, v) { return u[0] * v[0] + u[1] * v[1] + u[2] * v[2]; }
        static dist(u, v) { return Math.hypot(u[0] - v[0], u[1] - v[1], u[2] - v[2]); }
        static dist2(u, v) { return (u[0] - v[0]) **2 + (u[1] - v[1]) ** 2 + (u[2] - v[2]) ** 2; }
        //#endregion

        //#region Magnitude, and normalization.
        get magnitude() { return Math.hypot(this[0], this[1], this[2]); }
        get magnitude2() { return this[0] ** 2 + this[1] ** 2 + this[2] ** 2;}
        get normalized() {
            const q = this.magnitude2;
            if (q === 0) return new vec3(0, 0, 0);
            const r = 1 / Math.sqrt(q);
            return new vec3(u[0] * r, u[1] * r, u[2] * r);
        }
        static normalize(o, u) {
            const q = u.magnitude2;
            if (q === 0) return o.resetCoords();
            const r = 1 / Math.sqrt(q);
            return this.setCoords(u[0] * r, u[1] * r, u[2] * r);
        }
        normalize() {
            const q = this.magnitude2;
            if (q === 0) return this.resetCoords();
            return this.scale(1 / Math.sqrt(q));
        }
        //#endregion

        //#region Static arithmetic operations
        static add(o, u, v) { o[0] = u[0] + v[0]; o[1] = u[1] + v[1]; o[2] = u[2] + v[2]; return o; }
        static sub(o, u, v) { o[0] = u[0] - v[0]; o[1] = u[1] - v[1]; o[2] = u[2] - v[2]; return o; }
        static mul(o, u, v) { o[0] = u[0] * v[0]; o[1] = u[1] * v[1]; o[2] = u[2] * v[2]; return o; }
        static div(o, u, v) { o[0] = u[0] / v[0]; o[1] = u[1] / v[1]; o[2] = u[2] / v[2]; return o; }
        static scale(o, u, q) { o[0] = u[0] * q; o[1] = u[1] * q; o[2] = u[2] * q; return o; }
        static negate(o, u) { o[0] = -u[0]; o[1] = -u[1]; o[2] = -u[2]; return o; }
        //#endregion

        //#region Instance arithmetic operations
        add(u) { this[0] += u[0]; this[1] += u[1]; this[2] += u[2]; return this; }
        sub(u) { this[0] -= u[0]; this[1] -= u[1]; this[2] -= u[2]; return this; }
        mul(u) { this[0] *= u[0]; this[1] *= u[1]; this[2] *= u[2]; return this; }
        div(u) { this[0] /= u[0]; this[1] /= u[1]; this[2] /= u[2]; return this; }
        scale(q) { this[0] *= q; this[1] *= q; this[2] *= q; return this; }
        negate() { this[0] = -this[0]; this[1] = -this[1]; this[2] = -this[2]; return this; }
        //#endregion

        //#region Coords getters & setters
        get x() { return this[0]; }
        get y() { return this[1]; }
        get z() { return this[2]; }
        set x(value) { this[0] = value; }
        set y(value) { this[1] = value; }
        set z(value) { this[2] = value; }
        //#endregion

    }

    class vec4 extends Float32Array {

        constructor(x, y, z, w) { super(3);
            this[0] = x;
            this[1] = y;
            this[2] = z;
            this[3] = w;
        }

        setCoords(x, y, z, w) { this[0] = x; this[1] = y; this[2] = z; this[3] = w; return this; }
        resetCoords() { this[0] = 0; this[1] = 0; this[2] = 0; this[3] = 0; return this; }

        toString() {
            return `vec4(${
            this[0].toFixed(2) }, ${
            this[1].toFixed(2) }, ${
            this[2].toFixed(2) }, ${
            this[3].toFixed(2)
        })`;
        }

        static get ZERO() {
            return new vec4(0, 0, 0, 0);
        }

        static get I() {
            return new vec4(1, 0, 0, 0);
        }

        static get J() {
            return new vec4(0, 1, 0, 0);
        }

        static get K() {
            return new vec4(0, 0, 1, 0);
        }

        static get L() {
            return new vec4(0, 0, 0, 1);
        }

        static get UNIT() {
            return new vec4(1, 1, 1, 1);
        }

        assign(other) {
            this[0] = other[0];
            this[1] = other[1];
            this[2] = other[2];
            this[3] = other[3];
            return this;
        }

        clone() {
            return new vec4(this[0], this[1], this[2], this[3]);
        }

        static lerp(o, u, v, t) {
            const s = 1 - t;
            o[0] = u[0] * s + v[0] * t;
            o[1] = u[1] * s + v[1] * t;
            o[2] = u[2] * s + v[2] * t;
            o[3] = u[3] * s + v[3] * t;
            return o;
        }

        static addScaled(o, a, b, q) {
            o[0] = a[0] + b[0] * q;
            o[1] = a[1] + b[1] * q;
            o[2] = a[2] + b[2] * q;
            o[3] = a[3] + b[3] * q;
            return o;
        }

        addScaled(u, q) {
            this[0] += u[0] * q;
            this[1] += u[1] * q;
            this[2] += u[2] * q;
            this[3] += u[3] * q;
            return this;
        }

        //#region Dot, and distance
        static dot(u, v) { return u[0] * v[0] + u[1] * v[1] + u[2] * v[2] + u[3] * v[3]; }
        static dist(u, v) { return Math.hypot(u[0] - v[0], u[1] - v[1], u[2] - v[2], u[3] - v[3]); }
        static dist2(u, v) { return (u[0] - v[0]) **2 + (u[1] - v[1]) ** 2 + (u[2] - v[2]) ** 2 + (u[3] - v[3]) ** 2; }
        //#endregion

        //#region Magnitude, and normalization.
        get magnitude() { return Math.hypot(this[0], this[1], this[2], this[3]); }
        get magnitude2() { return this[0] ** 2 + this[1] ** 2 + this[2] ** 2 + this[3] ** 2;}
        get normalized() {
            const q = this.magnitude2;
            if (q === 0) return new vec4(0, 0, 0, 0);
            const r = 1 / Math.sqrt(q);
            return new vec4(u[0] * r, u[1] * r, u[2] * r, u[3] * r);
        }

        static normalize(o, u) {
            const q = u.magnitude2;
            if (q === 0) return o.resetCoords();
            const r = 1 / Math.sqrt(q);
            return this.setCoords(u[0] * r, u[1] * r, u[2] * r, u[3] * r);
        }

        normalize() {
            const q = this.magnitude2;
            if (q === 0) return this.resetCoords();
            return this.scale(1 / Math.sqrt(q));
        }

        //#endregion

        //#region Static arithmetic operations
        static add(o, u, v) { o[0] = u[0] + v[0]; o[1] = u[1] + v[1]; o[2] = u[2] + v[2]; o[3] = u[3] + v[3]; return o; }
        static sub(o, u, v) { o[0] = u[0] - v[0]; o[1] = u[1] - v[1]; o[2] = u[2] - v[2]; o[3] = u[3] - v[3]; return o; }
        static mul(o, u, v) { o[0] = u[0] * v[0]; o[1] = u[1] * v[1]; o[2] = u[2] * v[2]; o[3] = u[3] * v[3]; return o; }
        static div(o, u, v) { o[0] = u[0] / v[0]; o[1] = u[1] / v[1]; o[2] = u[2] / v[2]; o[3] = u[3] / v[3]; return o; }
        static scale(o, u, q) { o[0] = u[0] * q; o[1] = u[1] * q; o[2] = u[2] * q; o[3] = u[3] * q; return o; }
        static negate(o, u) { o[0] = -u[0]; o[1] = -u[1]; o[2] = -u[2]; o[3] = -u[3]; return o; }
        //#endregion

        //#region Instance arithmetic operations
        add(u) { this[0] += u[0]; this[1] += u[1]; this[2] += u[2]; this[3] += u[3]; return this; }
        sub(u) { this[0] -= u[0]; this[1] -= u[1]; this[2] -= u[2]; this[3] -= u[3]; return this; }
        mul(u) { this[0] *= u[0]; this[1] *= u[1]; this[2] *= u[2]; this[3] *= u[3]; return this; }
        div(u) { this[0] /= u[0]; this[1] /= u[1]; this[2] /= u[2]; this[3] /= u[3]; return this; }
        scale(q) { this[0] *= q; this[1] *= q; this[2] *= q; this[3] *= q; return this; }
        negate() { this[0] = -this[0]; this[1] = -this[1]; this[2] = -this[2]; this[3] = -this[3]; return this; }
        //#endregion

        //#region Coords getters & setters
        get x() { return this[0]; }
        get y() { return this[1]; }
        get z() { return this[2]; }
        get w() { return this[3]; }
        set x(value) { this[0] = value; }
        set y(value) { this[1] = value; }
        set z(value) { this[2] = value; }
        set w(value) { this[3] = value; }
        //#endregion

    }

    const aux$v3 = new vec3(0, 0, 0);
    const HalfPIOver180 = 0.5 * Math.PI / 180.0;

    class quat extends Float32Array {

        constructor(x, y, z, w) { super(4);
            this[0] = x;
            this[1] = y;
            this[2] = z;
            this[3] = w;
        }

        setParts(x, y, z, w) {
            this[0] = x;
            this[1] = y;
            this[2] = z;
            this[3] = w;
        }

        resetParts() {
            this[0] = 0;
            this[1] = 0;
            this[2] = 0;
            this[3] = 1;
        }

        toString() {
            return `quat(${
            this[0].toFixed(2) }, ${
            this[1].toFixed(2) }, ${
            this[2].toFixed(2) }, ${
            this[3].toFixed(2)
        })`;
        }

        static get IDENTITY() {
            return new quat(0, 0, 0, 1);
        }

        static FromEuler(out, x, y, z) {

            x *= HalfPIOver180;
            y *= HalfPIOver180;
            z *= HalfPIOver180;
        
            const sx = Math.sin(x);
            const cx = Math.cos(x);
            const sy = Math.sin(y);
            const cy = Math.cos(y);
            const sz = Math.sin(z);
            const cz = Math.cos(z);
        
            out[0] = sx * cy * cz - cx * sy * sz;
            out[1] = cx * sy * cz + sx * cy * sz;
            out[2] = cx * cy * sz - sx * sy * cz;
            out[3] = cx * cy * cz + sx * sy * sz;

            return out;

        }

        static invert(q, r) {
            const mag_sqr = vec4.dot(r, r);
            if(mag_sqr == 0) {
                q.fill(0);
                return q;
            }
            const inv_mag_sqr = 1 / mag_sqr;
            q[0] *= -inv_mag_sqr;
            q[1] *= -inv_mag_sqr;
            q[2] *= -inv_mag_sqr;
            q[3] *= inv_mag_sqr;
        }

        static rotateZ(o, q, r) {

            const theta = r * 0.5;
          
            const Nx = q[0], Ny = q[1], Nz = q[2], W = q[3];
            const sine = Math.sin(theta), cosine = Math.cos(theta);
          
            o[0] = Nx * cosine + Ny * sine;
            o[1] = Ny * cosine - Nx * sine;
            o[2] = Nz * cosine +  W * sine;
            o[3] =  W * cosine - Nz * sine;

            return o;

        }

        rotateZ(r) {

            const theta = r * 0.5;
          
            const Nx = this[0], Ny = this[1], Nz = this[2], W = this[3];
            const sine = Math.sin(theta), cosine = Math.cos(theta);
          
            this[0] = Nx * cosine + Ny * sine;
            this[1] = Ny * cosine - Nx * sine;
            this[2] = Nz * cosine +  W * sine;
            this[3] =  W * cosine - Nz * sine;

            return this;

        }

        static rotateY(o, q, r) {

            const theta = r * 0.5;
            const Nx = q[0], Ny = q[1], Nz = q[2], W = q[3];
            const sine = Math.sin(theta), cosine = Math.cos(theta);
          
            o[0] = Nx * cosine - Nz * sine; 
            o[1] = Ny * cosine +  W * sine;
            o[2] = Nz * cosine + Nx * sine;
            o[3] =  W * cosine - Ny * sine;

            return o;

        }

        rotateY(r) {

            const theta = r * 0.5;
            const Nx = this[0], Ny = this[1], Nz = this[2], W = this[3];
            const sine = Math.sin(theta), cosine = Math.cos(theta);

            this[0] = Nx * cosine - Nz * sine; 
            this[1] = Ny * cosine +  W * sine;
            this[2] = Nz * cosine + Nx * sine;
            this[3] =  W * cosine - Ny * sine;

            return this;

        }

        static rotateX(o, q, r) {
            const theta = r * 0.5;
            const Nx = q[0], Ny = q[1], Nz = q[2], W = q[3];
            const sine = Math.sin(theta), cosine = Math.cos(theta);
            o[0] = Nx * cosine +  W * sine;
            o[1] = Ny * cosine + Nz * sine;
            o[2] = Nz * cosine - Ny * sine;
            o[3] =  W * cosine - Nx * sine;
            return o;
        }

        rotateX(r) {
            const theta = r * 0.5;
            const Nx = this[0], Ny = this[1], Nz = this[2], W = this[3];
            const sine = Math.sin(theta), cosine = Math.cos(theta);
            this[0] = Nx * cosine +  W * sine;
            this[1] = Ny * cosine + Nz * sine;
            this[2] = Nz * cosine - Ny * sine;
            this[3] =  W * cosine - Nx * sine;
            return this;
        }

        getAxisAngle(axis) {
            const theta = Math.acos(this[3]);
            const sine = Math.sin(theta);
            if (sine !== 0) {
                axis[0] = q[0] / sine;
                axis[1] = q[1] / sine;
                axis[2] = q[2] / sine;
              } else {
                // If s is zero, return any axis (no rotation - axis does not matter)
                axis[0] = 1;
                axis[1] = 0;
                axis[2] = 0;
              }
            return theta * 2;
        }

        static FromAxisAngle(axis, r) {
            const theta = r / 2;
            const sine = Math.sin(theta);
            return new quat(
                axis[0] * sine,
                axis[1] * sine,
                axis[2] * sine,
                Math.cos(theta)
            );
        }

        static combine(q, r, s) {
            const Nr_x_Ns = vec3.cross(aux$v3, r, s);
            const Ws = s[3]; const Wr = r[3];
            q[0] = Nr_x_Ns[0] + Wr * s[0] + Ws * r[0];
            q[1] = Nr_x_Ns[1] + Wr * s[1] + Ws * r[1];
            q[2] = Nr_x_Ns[2] + Wr * s[2] + Ws * r[2];
            q[3] = vec3.dot(r, s) * Wr * Ws;
            return q;
        }

        static transform(o, q, u) {

            const x = u[0], y = u[1], z = u[2];

            const qx = q[0]; const qy = q[1];
            const qz = q[2]; const qw = q[3];

            const ix =  qw * x + qy * z - qz * y;
            const iy =  qw * y + qz * x - qx * z;
            const iz =  qw * z + qx * y - qy * x;
            const iw = -qx * x - qy * y - qz * z;

            o[0] = ix * qw + iw * -qx + iy * -qz - iz * -qy;
            o[1] = iy * qw + iw * -qy + iz * -qx - ix * -qz;
            o[2] = iz * qw + iw * -qz + ix * -qy - iy * -qx;

            return o;

        }

        clone() {
            return new quat(this[0], this[1], this[2], this[3]);
        }

        get normalized() {
            const q = this[0] ** 2 + this[1] ** 2 + this[2] ** 2 + this[3] ** 2;
            if (q === 0) return new quat(0, 0, 0, 0);
            const r = 1 / Math.sqrt(q);
            return new quat(u[0] * r, u[1] * r, u[2] * r, u[3] * r);
        }

        static normalize(o, u) {
            const q = u[0] ** 2 + u[1] ** 2 + u[2] ** 2 + u[3] ** 2;
            if (q === 0) {
                o.fill(0);
                return o;
            }
            const r = 1 / Math.sqrt(q);
            return this.setParts(u[0] * r, u[1] * r, u[2] * r, u[3] * r);
        }
        
        normalize() {
            const q = this[0] ** 2 + this[1] ** 2 + this[2] ** 2 + this[3] ** 2;
            if (q === 0) {
                this.fill(0);
                return this;
            }
            const r = 1 / Math.sqrt(q);
            this[0] *= r; this[1] *= r;
            this[2] *= r; this[3] *= r;
            return this;
        }

        get conjugated() {
            return new quat(-this[0], -this[1], -this[2], this[3]);
        }

        conjugate() {
            this[0] = -this[0];
            this[1] = -this[1];
            this[2] = -this[2];
            this[3] = this[3];
            return this;
        }

        static conjugate(q, r) {
            q[0] = -r[0];
            q[1] = -r[1];
            q[2] = -r[2];
            q[3] = r[3];
            return q;
        }

    }

    exports.vec2 = vec2;
    exports.vec3 = vec3;
    exports.vec4 = vec4;
    exports.quat = quat;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
