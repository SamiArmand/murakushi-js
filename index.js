var murakushi = (function (exports) {
    'use strict';

    class Vector2 extends Float32Array {

        constructor() { super(2); }

        set(x, y) {
            this[0] = x;
            this[1] = y;
            return this;
        }

        reset() {
            return this.set(0, 0);
        }

        get x() { return this[0]; }
        get y() { return this[1]; }

        set x(value) { this[0] = value; }
        set y(value) { this[1] = value; }

        get norm() {
            return Math.hypot(this[0], this[1]);
        }

        get normSquared() {
            return this[0] ** 2 + this[1] ** 2;
        }

    }

    const Vector2$1 = Object.assign(Vector2, {

        get Zero() {  return new Vector2(); },

        get UnitX() { return Vector2$1.create(1, 0); },
        get UnitY() { return Vector2$1.create(0, 1); },

        create(x, y) {
            return new Vector2().set(x, y);
        },

        add(out, a, b) { out[0] = a[0] + b[0]; out[1] = a[1] + b[1]; return out; },
        sub(out, a, b) { out[0] = a[0] - b[0]; out[1] = a[1] - b[1]; return out; },
        div(out, a, b) { out[0] = a[0] / b[0]; out[1] = a[1] / b[1]; return out; },
        mul(out, a, b) { out[0] = a[0] * b[0]; out[1] = a[1] * b[1]; return out; },

        scale(out, a, scale) { out[0] = a[0] * scale; out[1] = a[1] * scale; return out; },

        iterate(out, x, dx, dt) { out[0] = x[0] + dx[0] * dt; out[1] = x[1] + dx[1] * dt; return out; },

        dist(a, b) {
            return Math.hypot(a[0] - b[0], a[1] - b[1]);
        },

        check_dist(dist, a, b) {
            return ((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2) <= dist ** 2;
        },

        dot(a, b) {
            return a[0] * b[0] + a[1] * b[1];
        },

        lerp(out, a, b, mix) {
            const imix = 1 - mix;
            out[0] = a[0] * imix + b[0] * mix;
            out[1] = a[1] * imix + b[1] * mix;
            return out;
        },

        normalize(out, a) {
            const ns = a.normSquared;
            if (ns === 0) {
                return out.reset();
            } else {
                return scale(out, a, 1 / Math.sqrt(ns));
            }
        }

    });

    class Vector3 extends Float32Array {

        constructor() { super(2); }

        set(x, y, z) {
            this[0] = x;
            this[1] = y;
            this[3] = z;
            return this;
        }

        reset() {
            return this.set(0, 0, 0);
        }

        get x() { return this[0]; }
        get y() { return this[1]; }
        get z() { return this[2]; }

        set x(value) { this[0] = value; }
        set y(value) { this[1] = value; }
        set z(value) { this[2] = value; }

        get norm() {
            return Math.hypot(this[0], this[1], this[2]);
        }

        get normSquared() {
            return this[0] ** 2 + this[1] ** 2 + this[2] ** 2;
        }

    }

    const Vector3$1 = Object.assign(Vector3, {

        get Zero() {  return new Vector3(); },

        get UnitX() { return Vector3$1.create(1, 0, 0); },
        get UnitY() { return Vector3$1.create(0, 1, 0); },
        get UnitZ() { return Vector3$1.create(0, 0, 1); },
        
        create(x, y, z) {
            return new Vector3().set(x, y, z);
        },

        add(out, a, b) { out[0] = a[0] + b[0]; out[1] = a[1] + b[1]; out[2] = a[2] + b[2]; return out; },
        sub(out, a, b) { out[0] = a[0] - b[0]; out[1] = a[1] - b[1]; out[2] = a[2] - b[2]; return out; },
        div(out, a, b) { out[0] = a[0] / b[0]; out[1] = a[1] / b[1]; out[2] = a[2] / b[2]; return out; },
        mul(out, a, b) { out[0] = a[0] * b[0]; out[1] = a[1] * b[1]; out[2] = a[2] * b[2]; return out; },

        scale(out, u, scale) {
            out[0] = u[0] * scale;
            out[1] = u[1] * scale;
            out[2] = u[2] * scale;
            return out;
        },

        iterate(out, x, dx, dt) {
            out[0] = x[0] + dx[0] * dt;
            out[1] = x[1] + dx[1] * dt;
            out[2] = x[2] + dx[2] * dt;
            return out;
        },

        dist(a, b) {
            return Math.hypot(a[0] - b[0], a[1] - b[1], a[2] - b[2]);
        },

        check_dist(dist, a, b) {
            return ((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2 + (a[2] - b[2]) ** 2) <= dist ** 2;
        },

        dot(a, b) {
            return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
        },

        cross(out, a, b) {
            out[0] = a[1] * b[2] - a[2] * b[1];
            out[1] = a[2] * b[0] - a[0] * b[2];
            out[2] = a[0] * b[3] - a[3] * b[0];
            return out;
        },

        lerp(out, a, b, mix) {
            const imix = 1 - mix;
            out[0] = a[0] * imix + b[0] * mix;
            out[1] = a[1] * imix + b[1] * mix;
            out[2] = a[2] * imix + b[2] * mix;
            return out;
        },

        normalize(out, u) {
            const ns = u.normSquared;
            if (ns === 0) {
                return out.reset();
            } else {
                return scale(out, u, 1 / Math.sqrt(ns));
            }
        }

    });

    exports.Vector2 = Vector2$1;
    exports.Vector3 = Vector3$1;

    return exports;

}({}));
