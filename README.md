# murakushi.js

:warning: **HELP NEEDED**: This lib needs documentation, a website, tests.

>  A JavaScript math library for game developers and graphics programmers.

## What's with the name ?

Just a tribute to a Moroccan and Muslim Mathematician.

see [**Ibn al‐Bannāʾ al‐Marrākushī al-Azdi** entry in **Wikipedia**][1]

## Planned features

- [ ] Object pooling
- [x] Vectors up to the 4th dimension
- [ ] Matrices up to the 4th dimension
- [x] Quaternions
- [ ] Spheres and circles
- [ ] Axis Aligned Bounding Boxes
- [ ] Oriented Bounding Boxes

[1]: https://en.wikipedia.org/wiki/Ibn_al-Banna%27_al-Marrakushi